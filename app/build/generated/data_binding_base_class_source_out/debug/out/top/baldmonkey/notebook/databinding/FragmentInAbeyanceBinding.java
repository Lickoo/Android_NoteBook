// Generated by view binder compiler. Do not edit!
package top.baldmonkey.notebook.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;
import top.baldmonkey.notebook.R;

public final class FragmentInAbeyanceBinding implements ViewBinding {
  @NonNull
  private final RelativeLayout rootView;

  @NonNull
  public final ImageView addInAbeyAnce;

  @NonNull
  public final RecyclerView inAbeyanceRecyclerview;

  @NonNull
  public final EditText searchInAbeyanceEditText;

  private FragmentInAbeyanceBinding(@NonNull RelativeLayout rootView,
      @NonNull ImageView addInAbeyAnce, @NonNull RecyclerView inAbeyanceRecyclerview,
      @NonNull EditText searchInAbeyanceEditText) {
    this.rootView = rootView;
    this.addInAbeyAnce = addInAbeyAnce;
    this.inAbeyanceRecyclerview = inAbeyanceRecyclerview;
    this.searchInAbeyanceEditText = searchInAbeyanceEditText;
  }

  @Override
  @NonNull
  public RelativeLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static FragmentInAbeyanceBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static FragmentInAbeyanceBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.fragment_in_abeyance, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static FragmentInAbeyanceBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.add_in_abey_ance;
      ImageView addInAbeyAnce = ViewBindings.findChildViewById(rootView, id);
      if (addInAbeyAnce == null) {
        break missingId;
      }

      id = R.id.in_abeyance_recyclerview;
      RecyclerView inAbeyanceRecyclerview = ViewBindings.findChildViewById(rootView, id);
      if (inAbeyanceRecyclerview == null) {
        break missingId;
      }

      id = R.id.search_in_abeyance_editText;
      EditText searchInAbeyanceEditText = ViewBindings.findChildViewById(rootView, id);
      if (searchInAbeyanceEditText == null) {
        break missingId;
      }

      return new FragmentInAbeyanceBinding((RelativeLayout) rootView, addInAbeyAnce,
          inAbeyanceRecyclerview, searchInAbeyanceEditText);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
