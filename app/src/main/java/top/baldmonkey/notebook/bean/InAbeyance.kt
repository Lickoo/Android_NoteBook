package top.baldmonkey.notebook.bean

/**
 * 这是待办实体类
 */
class InAbeyance {
    internal var _id // _id
            = 0
    var content // 待办内容
            : String? = null
    var date_remind // 提醒日期
            : String? = null
    var status // 完成状态
            = 0
    var date_created // 创建日期
            : String? = null

    constructor(_id: Int, content: String?, date_remind: String?) {
        this._id = _id
        this.content = content
        this.date_remind = date_remind
    }

    constructor(content: String?, date_remind: String?, date_created: String?) {
        this.content = content
        this.date_remind = date_remind
        this.date_created = date_created
    }

    constructor(_id: Int, content: String?, date_remind: String?, status: Int) {
        this._id = _id
        this.content = content
        this.date_remind = date_remind
        this.status = status
    }

    fun get_id(): Int {
        return _id
    }

    fun set_id(_id: Int) {
        this._id = _id
    }

    override fun toString(): String {
        return "InAbeyance{" +
                "_id=" + _id +
                ", content='" + content + '\'' +
                ", date_created='" + date_created + '\'' +
                ", date_remind='" + date_remind + '\'' +
                ", status=" + status +
                '}'
    }
}