package top.baldmonkey.notebook.bean

/**
 * 这是笔记实体类
 */
class Note {
    internal var _id // id
            : Int? = null
    var title // 标题
            : String? = null
    var content // 内容
            : String? = null
    var date_created // 创建日期
            : String? = null
    var date_updated // 更新日期
            : String? = null
    var recycle_status // 是否处于回收状态 0 代表未回收, 1 代表已回收
            = 0

    constructor() {}
    constructor(title: String?, content: String?) {
        this.title = title
        this.content = content
    }

    constructor(_id: Int?, title: String?, content: String?) {
        this._id = _id
        this.title = title
        this.content = content
    }

    constructor(title: String?, content: String?, date_created: String?, date_updated: String?) {
        this.title = title
        this.content = content
        this.date_created = date_created
        this.date_updated = date_updated
    }

    constructor(
        _id: Int?,
        title: String?,
        content: String?,
        date_created: String?,
        date_updated: String?
    ) {
        this._id = _id
        this.title = title
        this.content = content
        this.date_created = date_created
        this.date_updated = date_updated
    }

    fun get_id(): Int? {
        return _id
    }

    fun set_id(_id: Int?) {
        this._id = _id
    }

    override fun toString(): String {
        return "Note{" +
                "_id=" + _id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", date_created='" + date_created + '\'' +
                ", date_updated='" + date_updated + '\'' +
                ", recycle_status=" + recycle_status +
                '}'
    }
}