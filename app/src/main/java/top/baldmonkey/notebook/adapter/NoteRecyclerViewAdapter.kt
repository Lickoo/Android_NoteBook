package top.baldmonkey.notebook.adapter

import androidx.recyclerview.widget.RecyclerView
import top.baldmonkey.notebook.adapter.NoteRecyclerViewAdapter.MyRecyclerViewHolder
import android.view.ViewGroup
import top.baldmonkey.notebook.R
import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.View.OnLongClickListener
import top.baldmonkey.notebook.util.DBOperator
import android.widget.Toast
import android.widget.TextView
import top.baldmonkey.notebook.bean.Note

/**
 * 这是继承自 RecyclerView.Adapter 的
 * 自定义的笔记的适配器 MyRecyclerViewAdapter
 */
class NoteRecyclerViewAdapter// 获取数据
// 获取上下文
// 构造器
    (
// 存放笔记数据
    var notes: MutableList<Note>, // 上下文
    var context: Context
) : RecyclerView.Adapter<MyRecyclerViewHolder>() {
    private lateinit var onItemClickListener: OnItemClickListener  // note item 单击事件监听器

    // 创建 ViewHolder
    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ): MyRecyclerViewHolder {
        // 获取笔记条目视图
        val itemView = View.inflate(context, R.layout.item_note, null)
        return MyRecyclerViewHolder(itemView)
    }

    // 为 ViewHolder 绑定数据
    override fun onBindViewHolder(
        holder: MyRecyclerViewHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {
        // 获取标题内容
        val title = notes[position].title
        val content = notes[position].content
        if (title?.length == 0) {
            holder.note_title.visibility = View.GONE
        } else {
            holder.note_title.visibility = View.VISIBLE
            holder.note_title.text = title
        }
        if (content?.length == 0) {
            holder.note_content.visibility = View.GONE
        } else {
            holder.note_content.visibility = View.VISIBLE
            holder.note_content.text = content
        }
        // 设置笔记更新日期
        holder.note_date.text = notes[position].date_updated
        // 设置笔记条目单击事件监听 --> 点击进入笔记修改界面
        holder.itemView.setOnClickListener {
            // 调用单击事件监听器
            if (onItemClickListener != null) {
                // 获取笔记标题和内容
                val title = notes[position].title
                val content = notes[position].content
                // 获取笔记的创建日期和id
                val date_created = notes[position].date_created
                val note_id = notes[position]._id.toString() + ""
                // 调用监听器的处理方法
                onItemClickListener!!.onItemClick(position, title, content, date_created, note_id)
            }
        }
        // 为笔记条目设置长按事件监听器 --> 长按回收
        holder.itemView.setOnLongClickListener {
            recycleNote(position) // 回收笔记
            true
        }
    }

    // 获取数据的总数
    override fun getItemCount(): Int {
        // 获取笔记数目
        return notes.size
    }

    // 回收一条笔记
    fun recycleNote(position: Int) {
        // 获取要回收的笔记的_id
        val _id = notes[position]._id
        // 从数据库中更改回收状态
        if (_id != null) {
            DBOperator.changeNoteRecycleStatus(context, _id)
        }
        notifyItemRemoved(position) // 通知哪一条数据数据被回收
        notes.removeAt(position) // 在条目中移除被回收的笔记
        // 通知适配器受到影响的position的位置
        notifyItemRangeChanged(position, notes.size - position)
        Toast.makeText(context, "移入回收站", Toast.LENGTH_SHORT).show()
    }

    fun updateNote(){
        notifyDataSetChanged()
    }

    // 设置单击事件监听器
    fun setOnItemClickListener(listener: OnItemClickListener?) {
        if (listener != null) {
            onItemClickListener = listener
        }
    }

    /**
     * 这是继承自 RecyclerView.ViewHolder 的
     * 自定义的 MyRecyclerViewHolder
     * 也是
     */
    class MyRecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @JvmField
        val note_title // 笔记标题
                : TextView
        @JvmField
        val note_content // 笔记内容
                : TextView
        @JvmField
        val note_date // 笔记修改日期
                : TextView

        init {
            // 获取控件
            note_title = itemView.findViewById(R.id.note_title)
            note_content = itemView.findViewById(R.id.note_content)
            note_date = itemView.findViewById(R.id.note_date)
        }
    }

    // note item 单击事件监听接口
    interface OnItemClickListener {
        fun onItemClick(
            position: Int,
            title: String?,
            content: String?,
            date_created: String?,
            note_id: String?
        )
    }
}