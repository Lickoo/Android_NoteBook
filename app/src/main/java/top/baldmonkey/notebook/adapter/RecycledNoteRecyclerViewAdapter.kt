package top.baldmonkey.notebook.adapter

import androidx.recyclerview.widget.RecyclerView
import top.baldmonkey.notebook.adapter.NoteRecyclerViewAdapter.MyRecyclerViewHolder
import android.view.ViewGroup
import top.baldmonkey.notebook.R
import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.View.OnLongClickListener
import top.baldmonkey.notebook.util.DBOperator
import android.widget.Toast
import top.baldmonkey.notebook.bean.Note

/**
 * 这是继承自 RecyclerView.Adapter 的
 * 自定义的 RecycledNoteRecyclerViewAdapter
 * 用于回收笔记展示的适配器
 * ViewHolder 采用的还是 NoteRecyclerViewAdapter 中写的 MyRecyclerViewHolder
 */
class RecycledNoteRecyclerViewAdapter     // 构造器
    (
// 存放已被回收的笔记
    var notes: MutableList<Note>, // 上下文
    var context: Context
) : RecyclerView.Adapter<MyRecyclerViewHolder>() {
    // 创建 ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyRecyclerViewHolder {
        val itemView = View.inflate(context, R.layout.item_note, null)
        return MyRecyclerViewHolder(itemView)
    }

    override fun onBindViewHolder(
        holder: MyRecyclerViewHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {
        // 获取标题内容
        val title = notes[position].title
        val content = notes[position].content
        if (title?.length == 0) {
            holder.note_title.visibility = View.GONE
        } else {
            holder.note_title.visibility = View.VISIBLE
            holder.note_title.text = title
        }
        if (content?.length == 0) {
            holder.note_content.visibility = View.GONE
        } else {
            holder.note_content.visibility = View.VISIBLE
            holder.note_content.text = content
        }
        // 设置笔记更新日期
        holder.note_date.text = notes[position].date_updated
        // 设置笔记条目单击事件监听 --> 点击将笔记还原
        holder.itemView.setOnClickListener { restoreNote(position) }
        // 为笔记条目设置长按事件监听器 --> 长按删除
        holder.itemView.setOnLongClickListener {
            deleteNote(position)
            true
        }
    }

    // 获取笔记条数
    override fun getItemCount(): Int {
        return notes.size
    }

    // 还原一条笔记
    fun restoreNote(position: Int) {
        // 获取要删除的笔记的_id
        val _id = notes[position]._id
        // 从数据库中删除
        if (_id != null) {
            DBOperator.changeNoteRecycleStatus(context, _id)
        }
        notifyItemRemoved(position) // 通知哪一条数据数据被回收
        notes.removeAt(position) // 在条目中移除被回收的笔记
        // 通知适配器受到影响的position的位置
        notifyItemRangeChanged(position, notes.size - position)
        Toast.makeText(context, "恢复成功", Toast.LENGTH_SHORT).show()
    }

    // 删除一条笔记
    fun deleteNote(position: Int) {
        // 获取要删除的笔记的_id
        val _id = notes[position]._id
        // 从数据库中删除
        if (_id != null) {
            DBOperator.deleteNote(context, _id)
        }
        notifyItemRemoved(position) // 通知哪一条数据数据被回收
        notes.removeAt(position) // 在条目中移除被回收的笔记
        // 通知适配器受到影响的position的位置
        notifyItemRangeChanged(position, notes.size - position)
        Toast.makeText(context, "删除成功", Toast.LENGTH_SHORT).show()
    }
}