package top.baldmonkey.notebook.adapter

import top.baldmonkey.notebook.bean.InAbeyance
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.view.LayoutInflater
import top.baldmonkey.notebook.R
import android.annotation.SuppressLint
import android.content.Context
import android.view.View.OnLongClickListener
import androidx.annotation.RequiresApi
import android.os.Build
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import top.baldmonkey.notebook.util.DBOperator
import top.baldmonkey.notebook.util.AlarmUtil
import android.widget.Toast

/**
 * 这是继承自 RecyclerView.Adapter 的
 * 自定义的待办的 RecyclerView 的适配器 InAbeyanceAdapter
 */
class InAbeyanceRecyclerViewAdapter// 获取数据
// 获取上下文
// 构造器
    (
// 待办数据
    var inAbeyances: MutableList<InAbeyance>, // 上下文
    var context: Context
) : RecyclerView.Adapter<InAbeyanceRecyclerViewAdapter.ViewHolder>() {
    private lateinit var onItemClickListener : OnItemClickListener  // InAbeyance item 单击事件监听接口

    // 创建ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // 获取待办item的视图
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_in_abeyance, parent, false)
        return ViewHolder(itemView)
    }

    // 绑定
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(
        holder: ViewHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {
        // 设置待办完成状态
        // status为 1 则代表已完成
        // 默认为不勾选
        holder.accomplish_status.isSelected = inAbeyances[position].status == 1
        // 给待办完成状态框添加点击事件监听
        holder.parent_of_accomplish_status.setOnClickListener {
            updateInabeyanceStatus(position) // 更新完成状态
        }
        // 设置待办内容
        holder.in_abeyance_content.text = inAbeyances[position].content
        // 设置闹钟图标显示状态并且设置提醒日期
        if ("" != inAbeyances[position].date_remind) {
            holder.hasRemind.isSelected = true
            // 显示为提醒日期
            holder.date_remind.text = inAbeyances[position].date_remind
        } else {
            holder.hasRemind.isSelected = false
            holder.date_remind.text = ""
        }
        // 设置设置待办条目主要内容区域长按事件监听 --> 长按删除
        holder.in_abeyance_main.setOnLongClickListener {
            deleteInAbeyance(position) // 删除该条待办
            true
        }
        // 设置待办条目主要内容区域点击事件监听 --> 点击可以进入待办修改界面
        holder.in_abeyance_main.setOnClickListener { // 获取待办数据
            val inAbeyance = inAbeyances[position]
            // 调用自定义的点击监听方法
            onItemClickListener!!.OnItemClick(
                position, inAbeyance._id.toString() + "",
                inAbeyance.content, inAbeyance.date_remind)
        }
    }

    override fun getItemCount(): Int {
        // 获取待办的数目用于控制条目数量
        return inAbeyances.size
    }

    /**
     * 这是继承自 RecyclerView.ViewHolder 的
     * 自定义的 ViewHolder
     * 也是 InAbeyanceAdapter 的内部类
     */
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val parent_of_accomplish_status // 待办完成状态选择框的容器
                : LinearLayout
        val accomplish_status // 待办完成状态
                : ImageView
        val in_abeyance_main // 待办信息主要布局
                : LinearLayout
        val in_abeyance_content // 待办内容
                : TextView
        val hasRemind // 待办是否有提醒
                : ImageView
        val date_remind // 待办提醒日期
                : TextView

        init {
            parent_of_accomplish_status = itemView.findViewById(R.id.in_abeyance_status)
            accomplish_status = itemView.findViewById(R.id.in_abeyance_check_box)
            in_abeyance_main = itemView.findViewById(R.id.in_abeyance_main)
            in_abeyance_content = itemView.findViewById(R.id.in_abeyance_content)
            hasRemind = itemView.findViewById(R.id.in_abeyance_alarm)
            date_remind = itemView.findViewById(R.id.date_remind)
        }
    }

    // 更新待办的完成状态
    fun updateInabeyanceStatus(position: Int) {
        val _id = inAbeyances[position]._id // 获取要删除的待办的_id
        // 更改该条待办数据的 status
        inAbeyances[position].status = 1 - inAbeyances[position].status
        notifyItemChanged(position) // 通知适配器该条数据已更改
        // 更新数据库中的数据
        DBOperator.changeInAbeyanceStatus(context, _id)
    }

    // 删除一条待办
    @RequiresApi(api = Build.VERSION_CODES.M)
    fun deleteInAbeyance(position: Int) {
        val _id = inAbeyances[position]._id // 获取要删除的待办的_id
        // 获取提醒日期，用于判断是否需要取消对应的闹钟
        val date_remind = inAbeyances[position].date_remind
        if ("" != date_remind) {
            AlarmUtil.cancelAlarm(context.applicationContext, _id)
        }
        // 在数据库中删除该待办
        DBOperator.deleteInAbeyance(context, _id) // 执行删除操作
        inAbeyances.removeAt(position) // 从数据中移除
        notifyItemRemoved(position) // 通知适配器该条数据已移除
        // 通知适配器 position 受到影响的范围
        notifyItemRangeChanged(position, inAbeyances.size - position)
        Toast.makeText(context, "删除成功", Toast.LENGTH_SHORT).show()
    }

    // 设置条目单击事件监听器
    fun setOnItemClickListener(onItemClickListener: OnItemClickListener?) {
        if (onItemClickListener != null) {
            this.onItemClickListener = onItemClickListener
        }
    }

    // InAbeyance item 单击事件监听接口
    interface OnItemClickListener {
        fun OnItemClick(position: Int, _id: String?, content: String?, date_remind: String?)
    }
}