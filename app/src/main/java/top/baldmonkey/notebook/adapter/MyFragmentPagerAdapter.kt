package top.baldmonkey.notebook.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter

/**
 * 这是继承自 FragmentStateAdapter 的
 * 自定义的 MyFragmentPagerAdapter
 */
class MyFragmentPagerAdapter// 为Fragment数组赋值     // 初始化
    (
    fragmentManager: FragmentManager,
    lifecycle: Lifecycle,
    // Fragment数组
    private var fragmentList: MutableList<Fragment>
) : FragmentStateAdapter(fragmentManager, lifecycle) {
    // 根据 position 创建 Fragment
    override fun createFragment(position: Int): Fragment {
        return fragmentList[position]
    }

    // 获取 Fragment 总数
    override fun getItemCount(): Int {
        return fragmentList.size
    }
}