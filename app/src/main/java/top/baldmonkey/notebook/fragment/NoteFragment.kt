package top.baldmonkey.notebook.fragment

import android.annotation.SuppressLint
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import top.baldmonkey.notebook.adapter.NoteRecyclerViewAdapter
import android.os.Bundle
import androidx.annotation.RequiresApi
import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import top.baldmonkey.notebook.R
import android.text.TextWatcher
import android.text.Editable
import android.content.Intent
import android.view.View
import android.widget.ImageView
import top.baldmonkey.notebook.activity.EditActivity
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import top.baldmonkey.notebook.bean.Note
import top.baldmonkey.notebook.util.DBOperator

/**
 * 这是继承自Fragment的自定义的NoteFragment类
 * 可以通过newInstance方法获取该NoteFragment实例
 */
class NoteFragment private constructor() : Fragment() {
    private var rootView // rootView 根视图
            : View? = null
    private var notes // 笔记数据
            : MutableList<Note>? = null
    var search_note // 搜索框
            : EditText? = null
    var recyclerView // RecyclerView
            : RecyclerView? = null
    var myRecyclerViewAdapter // RecyclerView适配器
            : NoteRecyclerViewAdapter? = null
    var add_note // 添加按钮
            : ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    /**
     * 通过这个方法来初始化NoteFragment的根视图
     * 并且在通过返回键返回该Fragment所在Activity时也会调用
     * @param inflater LayoutInflater
     * @param container ViewGroup
     * @param savedInstanceState Bundle
     * @return View rootView 这是这个Fragment的根视图
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_note, container, false)
            initView()
        }
        return rootView
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    override fun onResume() {
        super.onResume()
        //        Log.e("xxx", "onResume: xxx" );
        initView()
    }

    // 初始化该Fragment的控件
    @RequiresApi(api = Build.VERSION_CODES.O)
    fun initView() {
//        Log.e("xxx", "NoteFragment--->initView: 走到这里");
        // 获取搜索框控件
        search_note = rootView!!.findViewById(R.id.search_note_editText)


        // 为搜索框添加事件监听,在搜索框的值改变时触发
        search_note?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            // 在内容改变后触发该方法
            override fun afterTextChanged(editable: Editable) {
                // 获取搜索框内容作为搜索关键字
                val keyWord = search_note?.getText().toString()
                // 查询与关键字匹配的笔记
                notes = queryNotesData(keyWord)
                // 更新所绑定的数据
                bindData()
            }
        })

        // 获取全部Note数据
        notes = notesData
        bindData() // 将数据绑定到RecyclerView中

        // 获取添加按钮控件
        add_note = rootView!!.findViewById(R.id.add_note)
        // 为添加按钮添加单击事件监听
        add_note?.setOnClickListener(View.OnClickListener {
            val intent = Intent(activity, EditActivity::class.java)
            startActivity(intent)
        })
    }

    // 将数据绑定到RecyclerView中
    @SuppressLint("UseRequireInsteadOfGet")
    fun bindData() {
        // 获取RecyclerView
        recyclerView = rootView!!.findViewById(R.id.note_recyclerview)
        // 采用瀑布网格布局
        val layoutManager = StaggeredGridLayoutManager(2, LinearLayout.VERTICAL)
        // 设置布局管理器
        recyclerView?.setLayoutManager(layoutManager)
        // 新建适配器
        myRecyclerViewAdapter = NoteRecyclerViewAdapter(notes!!, activity!!)
        // 为适配器的每个条目设置单击事件监听
        // 与 MyRecyclerViewAdapter 中的 OnItemClickListener 接口结合使用
        myRecyclerViewAdapter!!.setOnItemClickListener(object :
            NoteRecyclerViewAdapter.OnItemClickListener {
            override fun onItemClick(
                position: Int,
                title: String?,
                content: String?,
                date_created: String?,
                note_id: String?
            ) {
                val bundle = Bundle() // 新建一个 Bundle 来传递数据
                bundle.putString("note_title", title) // 放入笔记标题
                bundle.putString("note_content", content) // 放入笔记内容
                bundle.putString("date_created", date_created) // 放入创建日期
                bundle.putString("note_id", note_id) // 放入笔记 _id
                // 创建意图,跳转到 EditActivity
                val intent = Intent(activity, EditActivity::class.java)
                intent.putExtras(bundle) // 将 Bundle 添加到 Intent 中
                startActivity(intent) // 进行跳转
            }
        })
        // 为RecyclerView设置适配器
        recyclerView?.setAdapter(myRecyclerViewAdapter)
    }// 查询全部Note数据并返回

    // 通过这个方法来获取全部Note数据
    val notesData: MutableList<Note>
        get() =// 查询全部Note数据并返回
            DBOperator.getNotesData(activity, 0)

    // 通过这个方法来查询符合关键字的note数据
    fun queryNotesData(keyWord: String?): MutableList<Note>? {
        // 按照关键字查询Note并返回
        return DBOperator.queryNotesData(activity, keyWord!!, 0)
    }

    companion object {
        /**
         * 通过这个方法可以在外部获取NoteFragment类的实例
         * @return NoteFragment
         */
        fun newInstance(): NoteFragment {
            return NoteFragment()
        }
    }
}