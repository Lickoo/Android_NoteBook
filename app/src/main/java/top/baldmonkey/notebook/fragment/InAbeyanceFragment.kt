package top.baldmonkey.notebook.fragment

import android.annotation.SuppressLint
import top.baldmonkey.notebook.bean.InAbeyance
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import top.baldmonkey.notebook.adapter.InAbeyanceRecyclerViewAdapter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import top.baldmonkey.notebook.R
import android.text.TextWatcher
import android.text.Editable
import android.content.Intent
import android.view.View
import android.widget.ImageView
import androidx.fragment.app.Fragment
import top.baldmonkey.notebook.activity.InAbeyanceDialogActivity
import androidx.recyclerview.widget.LinearLayoutManager
import top.baldmonkey.notebook.util.DBOperator

/**
 * 这是继承自 Fragment 的
 * 自定义的 InAbeyanceFragment类
 */
class InAbeyanceFragment private constructor() : Fragment() {
    private var rootView // 根视图
            : View? = null
    var inAbeyances // 存放 InAbeyance 数据
            : MutableList<InAbeyance>? = null
    var search_inAbeyance // 搜索框
            : EditText? = null
    var recyclerView // RecyclerView控件
            : RecyclerView? = null
    var inAbeyanceRecyclerViewAdapter // 适配器
            : InAbeyanceRecyclerViewAdapter? = null
    var add_inAbeyance // 添加待办按钮
            : ImageView? = null

    // 初始化
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    /**
     * 初始化根视图 rootView
     * 并且在通过返回键返回该 Fragment 所在 Activity 时也会调用
     * @param inflater LayoutInflater
     * @param container ViewGroup
     * @param savedInstanceState Bundle
     * @return rootView
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_in_abeyance, container, false)
            initView()
        }
        return rootView
    }

    // 在返回到该Fragment时调用
    override fun onResume() {
        super.onResume()
        initView()
    }

    // 初始化根视图的控件
    private fun initView() {
        // 获取搜索框
        search_inAbeyance = rootView!!.findViewById(R.id.search_in_abeyance_editText)
        // 为搜索框添加文本改变监听事件
        search_inAbeyance?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            // 输入框内容改变后触发
            override fun afterTextChanged(editable: Editable) {
                val keyWord = search_inAbeyance?.getText().toString()
                inAbeyances = queryInAbeyancesData(keyWord)
                bindData()
            }
        })
        // 获取全部待办数据
        inAbeyances = inAbeyancesData
        bindData() // 绑定数据
        // 获取添加按钮
        add_inAbeyance = rootView!!.findViewById(R.id.add_in_abey_ance)
        // 为添加按钮添加单击事件监听 --> 单击到达添加待办页面
        add_inAbeyance?.setOnClickListener(View.OnClickListener { // 创建意图
            val intent = Intent(activity, InAbeyanceDialogActivity::class.java)
            // 执行意图
            startActivity(intent)
        })

    }

    // 绑定数据
    @SuppressLint("UseRequireInsteadOfGet")
    private fun bindData() {
        // 获取RecyclerView
        recyclerView = rootView!!.findViewById(R.id.in_abeyance_recyclerview)
        // 设置为线性布局
        val linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerView?.setLayoutManager(linearLayoutManager)
        // 创建适配器
        inAbeyanceRecyclerViewAdapter = InAbeyanceRecyclerViewAdapter(inAbeyances!!, activity!!)
        // 为待办条目设置单击事件监听器
        inAbeyanceRecyclerViewAdapter!!.setOnItemClickListener(object :
            InAbeyanceRecyclerViewAdapter.OnItemClickListener {
            override fun OnItemClick(
                position: Int,
                _id: String?,
                content: String?,
                date_remind: String?
            ) {
                // 创建 Bundle
                val bundle = Bundle()
                // 添加数据
                bundle.putString("_id", _id)
                bundle.putString("content", content)
                bundle.putString("date_remind", date_remind)
                // 创建意图
                val intent = Intent(activity, InAbeyanceDialogActivity::class.java)
                // 将 bundle 放入 intent
                intent.putExtras(bundle)
                startActivity(intent) // 执行意图
            }
        })
        // 设置适配器
        recyclerView?.setAdapter(inAbeyanceRecyclerViewAdapter)
    }// 在数据库中全部查询待办数据

    // 获取全部待办信息
    val inAbeyancesData: MutableList<InAbeyance>
        get() =// 在数据库中全部查询待办数据
            DBOperator.getInAbeyanceData(activity)

    // 根据关键字查询信息
    fun queryInAbeyancesData(keyWord: String?): MutableList<InAbeyance> {
        // 根据关键字在数据库中进行搜索待办
        return DBOperator.queryInAbeyanceData(activity, keyWord!!)
    }

    companion object {
        /**
         * 通过这个方法在外界获取 InAbeyanceFragment 实例
         * @return InAbeyanceFragment fragment
         */
        fun newInstance(): InAbeyanceFragment {
            return InAbeyanceFragment()
        }
    }
}