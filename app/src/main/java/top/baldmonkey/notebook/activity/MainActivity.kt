package top.baldmonkey.notebook.activity

import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import android.widget.TextView
import androidx.annotation.RequiresApi
import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import top.baldmonkey.notebook.R
import android.content.Intent
import top.baldmonkey.notebook.activity.RecycleBinActivity
import top.baldmonkey.notebook.activity.MainActivity
import top.baldmonkey.notebook.fragment.NoteFragment
import top.baldmonkey.notebook.fragment.InAbeyanceFragment
import top.baldmonkey.notebook.adapter.MyFragmentPagerAdapter
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import android.annotation.SuppressLint
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import top.baldmonkey.notebook.util.DBOperator
import top.baldmonkey.notebook.util.DataDownloader
import top.baldmonkey.notebook.util.DataUpdater
import java.util.ArrayList

/**
 * 这是继承自 AppCompatActivity 的
 * 实现了 View.OnClickListener 点击事件接口的 MainActivity
 * 也是程序的入口
 */
class MainActivity : AppCompatActivity(), View.OnClickListener {
    var viewPager // ViewPager2
            : ViewPager2? = null
    var note // 顶部笔记标签
            : TextView? = null
    var in_abeyance // 顶部待办标签
            : TextView? = null
    var recycle_bin // 回收站按钮
            : ImageView? = null
    var update_btn // 上传按钮
            : ImageView? = null
    var download_btn // 下载按钮
            : ImageView? = null
    var add_btn // 下载按钮
            : FloatingActionButton? = null

    // 初始化
    @RequiresApi(api = Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        super.onCreate(savedInstanceState)
        // 透明状态栏
        val window = window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = resources.getColor(R.color.light_grey)
        // 设置状态栏字体颜色
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        setContentView(R.layout.activity_main)
        note = findViewById(R.id.note) // 获取笔记文本控件
        note?.setOnClickListener(this) // 添加单击事件监听
        in_abeyance = findViewById(R.id.in_abeyance) // 获取待办文本控件
        in_abeyance?.setOnClickListener(this) // 添加单击事件监听
        recycle_bin = findViewById(R.id.recycle_bin)
        // 给回收站按钮添加点击事件
        recycle_bin?.setOnClickListener(View.OnClickListener { // 创建意图
            val intent = Intent(
                this@MainActivity, RecycleBinActivity::class.java)
            // 执行意图,跳转到回收站页面
            startActivity(intent)
        })
        update_btn = findViewById(R.id.update_btn)
        update_btn?.setOnClickListener {
            val dialogBuilder = AlertDialog.Builder(this)
            dialogBuilder.setTitle("上传笔记数据")
            dialogBuilder.setMessage("确定是否需要上传笔记数据?")
            dialogBuilder.setPositiveButton("确定"){
                dialog,which->DataUpdater().Update(this)
            }
            dialogBuilder.setNegativeButton("取消"){
                dialog,which->{}
            }
            dialogBuilder.create().show()  //创建模态框
        }
        download_btn = findViewById(R.id.download_btn)
        download_btn?.setOnClickListener {

            val dialogBuilder = AlertDialog.Builder(this)
            dialogBuilder.setTitle("同步笔记数据")
            dialogBuilder.setMessage("确定是否需要同步并覆盖笔记数据?")
            dialogBuilder.setPositiveButton("确定"){
                    dialog,which->
                run {
                    val download_list = DataDownloader().DownloadNotes(this)
                    if (download_list != null) {  //如果为空，则不同步
                        DBOperator.deleteAllNote(this)
                        for (note in download_list) {
                            DBOperator.addNote(this, note)
                        }
                        initViewPager()  //刷新笔记页面
                    }
                }
            }
            dialogBuilder.setNegativeButton("取消"){
                    dialog,which->{}
            }
            dialogBuilder.create().show()  //创建模态框
        }
        //添加按钮，使用Fragment来判断
        add_btn = findViewById<FloatingActionButton>(R.id.add_button)
        add_btn?.setOnClickListener {
            if(fasterstatus == 0){
                val intent = Intent(this, EditActivity::class.java)
                // 执行意图
                startActivity(intent)
            }
            if(fasterstatus == 1){
                val intent = Intent(this, InAbeyanceDialogActivity::class.java)
                // 执行意图
                startActivity(intent)
            }

        }

        initViewPager() // 初始化 ViewPager2
    }

    override fun onResume() {
        super.onResume()
        // 重新返回本页面时设置显示的页面
        viewPager!!.currentItem = status
    }

    override fun onPause() {
        super.onPause()
        // 离开本页面时调用
        status = viewPager!!.currentItem
    }

    // 初始化 ViewPager2 方法
    private fun initViewPager() {
        // 获取 mainViewPager2
        viewPager = findViewById(R.id.mainViewPager2)
        // 创建 FragMent 数组
        val fragmentList: MutableList<Fragment> = ArrayList()
        // 添加笔记的 Fragment
        fragmentList.add(NoteFragment.newInstance())
        // 添加代办的 Fragment
        fragmentList.add(InAbeyanceFragment.newInstance())
        // 创建 Fragment 适配器
        val myFragmentPagerAdapter = MyFragmentPagerAdapter(supportFragmentManager,
            lifecycle, fragmentList)
        // 给 ViewPager2 设置适配器
        viewPager?.setAdapter(myFragmentPagerAdapter)
        // Fragment 切换事件监听
        viewPager?.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            @SuppressLint("ResourceAsColor")
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                changeFragment(position)
            }
        })
    }

    // 切换 Fragment
    @SuppressLint("NonConstantResourceId")
    private fun changeFragment(position: Int) {
        when (position) {
            R.id.note -> {
                viewPager!!.currentItem = 0
                note!!.setTextColor(note!!.resources.getColor(R.color.black))
                in_abeyance!!.setTextColor(in_abeyance!!.resources.getColor(R.color.dark_grey))
                recycle_bin!!.visibility = View.VISIBLE
            }
            0 -> {
                note!!.setTextColor(note!!.resources.getColor(R.color.black))
                in_abeyance!!.setTextColor(in_abeyance!!.resources.getColor(R.color.dark_grey))
                recycle_bin!!.visibility = View.VISIBLE
                update_btn!!.visibility = View.VISIBLE
                download_btn!!.visibility = View.VISIBLE
                fasterstatus = 0
            }
            R.id.in_abeyance -> {
                viewPager!!.currentItem = 1
                note!!.setTextColor(note!!.resources.getColor(R.color.dark_grey))
                in_abeyance!!.setTextColor(in_abeyance!!.resources.getColor(R.color.black))
                recycle_bin!!.visibility = View.INVISIBLE
                update_btn!!.visibility = View.INVISIBLE
                download_btn!!.visibility = View.INVISIBLE
            }
            1 -> {
                note!!.setTextColor(note!!.resources.getColor(R.color.dark_grey))
                in_abeyance!!.setTextColor(in_abeyance!!.resources.getColor(R.color.black))
                recycle_bin!!.visibility = View.INVISIBLE
                update_btn!!.visibility = View.INVISIBLE
                download_btn!!.visibility = View.INVISIBLE
                fasterstatus = 1
            }
            else -> {}
        }
    }

    // 重写点击事件 处理方法
    override fun onClick(view: View) {
        // 调用切换 Fragment 方法
        changeFragment(view.id)
    }

    companion object {
        // 标志当前显示笔记页面或者待办页面, 0 为笔记, 1 为待办
        var status = 0
        var fasterstatus = 0  //上面那个变量变化较慢，添加内容的时候容易反应不过来
    }
}