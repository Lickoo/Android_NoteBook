package top.baldmonkey.notebook.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.EditText
import androidx.annotation.RequiresApi
import android.os.Build
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import top.baldmonkey.notebook.R
import top.baldmonkey.notebook.activity.MainActivity
import top.baldmonkey.notebook.util.DBOperator
import android.widget.Toast
import top.baldmonkey.notebook.bean.Note
import top.baldmonkey.notebook.databinding.ActivityEditBinding

/**
 * 这是继承自 AppCompatActivity 的 EditActivity
 */
class EditActivity : AppCompatActivity() {
    var bundle // 传来的数据
            : Bundle? = null
    var date_created // 创建日期文本框
            : TextView? = null
    var note_id // 一个隐藏的文本框用于标记 note 的 _id
            : TextView? = null
    var title // 标题编辑框
            : EditText? = null
    var content // 内容编辑框
            : EditText? = null
    var backButton // 返回按钮
            : ImageView? = null
    var accomplishButton // 完成按钮
            : ImageView? = null
    var shareButton // 分享按钮
            : ImageView? = null

    private lateinit var binding:ActivityEditBinding

    // 初始化方法
    @RequiresApi(api = Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditBinding.inflate(layoutInflater)
        // 透明状态栏
        val window = window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = resources.getColor(R.color.white)
        // 设置字体颜色
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        setContentView(binding.root)
        // 初始化获取控件
        note_id = binding.noteId
        title = binding.noteTitleEdit
        content = binding.noteContentEdit
        backButton = binding.back
        accomplishButton = binding.accomplish
        shareButton = binding.shareBtn
        // 为返回按钮添加监听
        backButton?.setOnClickListener(View.OnClickListener { // 调用系统返回键
            MainActivity.status = 0
            onBackPressed()
        })
        // 为完成按钮添加监听
        accomplishButton?.setOnClickListener(View.OnClickListener { // 获取控件 _id,标题,内容
            val s_id = note_id?.getText().toString()
            val title = title?.getText().toString()
            val content = content?.getText().toString()
            // 声明一个 Note 对象
            val note: Note
            // 对内容进行判断
            if ("" == s_id && ("" != title || "" != content)) {
                // _id为空代表是添加笔记
                // 且不允许标题和内容同时为空
                note = Note(title, content)
                addNote(note) // 执行添加
            } else if ("" != s_id && ("" != title || "" != content)) {
                // _id不为空代表为修改数据
                // 且不允许标题和内容同时为空
                val _id = s_id.toInt()
                note = Note(_id, title, content)
                updateNote(note) // 执行更新
            }
            // 调用系统返回键
            onBackPressed()
        })
        bundle = intent.extras // 获取传入的笔记数据
        if (bundle != null) {
            // 将传来的数据与控件绑定
            date_created = findViewById(R.id.date_created)
            title?.setText(bundle!!.getString("note_title"))
            content?.setText(bundle!!.getString("note_content"))
            note_id?.setText(bundle!!.getString("note_id"))
            val tips = "创建于：" + bundle!!.getString("date_created")
            date_created?.setText(tips)
            bundle!!.clear() // 清除 bundle
        }

        shareButton?.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_TEXT,"标题:${title?.text}\n内容:\n${content?.text}\n${date_created?.text}\n")
            startActivity(Intent.createChooser(intent,"分享到..."))
        }
    }

    // 添加一条笔记
    fun addNote(note: Note?) {
        // 在数据库中添加笔记
        if (note != null) {
            DBOperator.addNote(this, note)
        }
        // 进行提示
        Toast.makeText(this, "添加成功", Toast.LENGTH_SHORT).show()
    }

    // 更新一条笔记
    fun updateNote(note: Note?) {
        // 在数据库中更新笔记
        if (note != null) {
            DBOperator.updateNote(this, note)
        }
        // 提示
        Toast.makeText(this, "更新成功", Toast.LENGTH_SHORT).show()
    }
}