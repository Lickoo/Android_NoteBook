package top.baldmonkey.notebook.activity

import androidx.appcompat.app.AppCompatActivity
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import androidx.annotation.RequiresApi
import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import top.baldmonkey.notebook.R
import android.text.TextWatcher
import android.text.Editable
import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import android.widget.LinearLayout
import top.baldmonkey.notebook.adapter.RecycledNoteRecyclerViewAdapter
import top.baldmonkey.notebook.bean.Note
import top.baldmonkey.notebook.util.DBOperator

/**
 * 这是继承自 AppCompatActivity 的
 * 自定义的 RecycleBinActivity
 */
class RecycleBinActivity : AppCompatActivity() {
    var notes // 存放回收的笔记数据
            : MutableList<Note>? = null
    var back // 返回按钮
            : ImageView? = null
    var recycle_note_search // 回收笔记搜索框
            : EditText? = null
    var recyclerView // 盛放回收笔记
            : RecyclerView? = null
    var deleteAllNotes // 盛放回收笔记
            : ImageView? = null

    // 初始化
    @RequiresApi(api = Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // 透明状态栏
        val window = window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = resources.getColor(R.color.light_grey)
        // 设置状态栏字体颜色
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        setContentView(R.layout.activity_recycle_bin)
        // 获取返回按钮
        back = findViewById(R.id.back_of_recycle_bin)
        // 为返回按钮添加点击事件监听
        back?.setOnClickListener(View.OnClickListener { onBackPressed() })
        // 获取搜索框
        recycle_note_search = findViewById(R.id.search_recycle_note_editText)
        // 给搜索框添加文本改变事件监听
        recycle_note_search?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            // 文本改变时触发
            override fun afterTextChanged(editable: Editable) {
                val keyWord = recycle_note_search?.getText().toString()
                // 获取数据
                notes = findNotesByKeyWord(keyWord)
                // 绑定数据
                bindData()
            }
        })
        deleteAllNotes = findViewById(R.id.delete_all_notes)
        deleteAllNotes?.setOnClickListener {
            DBOperator.deleteAllTrash(this)
            finish()
        }
        // 获取数据
        notes = allNotes
        // 绑定数据
        bindData()
    }

    // 绑定数据
    fun bindData() {
        // 获取RecyclerView
        recyclerView = findViewById(R.id.recycle_note_recyclerview)
        // 设置瀑布网格布局
        val layoutManager = StaggeredGridLayoutManager(2, LinearLayout.VERTICAL)
        recyclerView?.setLayoutManager(layoutManager)
        // 创建适配器
        val adapter = RecycledNoteRecyclerViewAdapter(notes!!, this)
        // 设置适配器
        recyclerView?.setAdapter(adapter)
    }

    // 查询全部已被回收的数据
    val allNotes: MutableList<Note>
        get() = DBOperator.getNotesData(this, 1)

    // 按照关键字查询已被回收的笔记
    fun findNotesByKeyWord(keyWord: String?): MutableList<Note> {
        return DBOperator.queryNotesData(this, keyWord!!, 1)
    }
}