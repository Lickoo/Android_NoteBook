package top.baldmonkey.notebook.util

import androidx.annotation.RequiresApi
import android.os.Build
import android.app.AlarmManager
import android.content.Intent
import top.baldmonkey.notebook.util.AlarmReceiver
import android.os.Bundle
import android.app.PendingIntent
import android.content.Context

/**
 * 这是一个闹钟工具类
 * 用于向系统注册或者取消闹钟
 * 来控制何时发出待办提醒通知
 */
object AlarmUtil {
    // 设置闹钟
    @RequiresApi(api = Build.VERSION_CODES.S)
    fun setAlarm(context: Context, remindTime: Long, _id: Int, content: String?) {
        // 获得系统提供的AlarmManager服务的对象
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        //Intent设置要启动的组件，这里启动广播接收器
        val intent = Intent(context, AlarmReceiver::class.java)
        // 创建Bundle 传入待办信息
        val bundle = Bundle()
        bundle.putString("content", content)
        intent.putExtras(bundle) // 将bundle放入intent中
        // PendingIntent对象设置动作,启动的是Activity还是Service,或广播!
        // 传入 _id 的作用是给闹钟定一个id,防止多个闹钟冲突
        val sender = PendingIntent.getBroadcast(
            context, _id, intent, PendingIntent.FLAG_IMMUTABLE)
        // 注册闹钟
        // RTC_WAKEUP：指定当系统调用System.currentTimeMillis()方法返回的值
        // 与triggerAtTime相等时启动operation所对应的设备
        // （在指定的时刻，发送广播，并唤醒设备）
        alarmManager[AlarmManager.RTC_WAKEUP, remindTime] = sender
    }

    // 取消闹钟
    @RequiresApi(api = Build.VERSION_CODES.M)
    fun cancelAlarm(context: Context, _id: Int) {
        // 创建意图
        val intent = Intent(context, AlarmReceiver::class.java)
        // 按照闹钟id取消闹钟
        val pendingIntent = PendingIntent.getBroadcast(
            context, _id, intent, PendingIntent.FLAG_IMMUTABLE)
        // 获取闹钟服务
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        // 取消闹钟
        alarmManager.cancel(pendingIntent)
    }
}