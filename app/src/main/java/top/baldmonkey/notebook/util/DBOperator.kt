package top.baldmonkey.notebook.util

import android.content.Context
import top.baldmonkey.notebook.util.DBHelper.Companion.getInstance
import top.baldmonkey.notebook.util.DateUtil.currentTime
import android.util.Log
import top.baldmonkey.notebook.bean.InAbeyance
import top.baldmonkey.notebook.bean.Note
import java.util.ArrayList

/**
 * 这是数据库操作的工具类
 * 需要配合DBHelper使用
 */
object DBOperator {
    // 通过这个方法来获取全部Note数据
    // 根据 recycle_status 决定是查询回收的还是未被回收的笔记
    // recycle_status = 0 代表未回收, recycle_status = 1 代表已回收
    fun getNotesData(context: Context?, recycle_status: Int): MutableList<Note> {
        val dbHelper = getInstance(context)
        // 获取数据库
        val database = dbHelper!!.readableDatabase
        // 编写全部查询的sql语句
        val getAllNotes = "select * from note " +
                "where recycle_status = " + recycle_status +
                " order by date_updated desc"
        val notes: MutableList<Note> = ArrayList() // 用于存放Note数据
        // 记录查询返回的游标
        val cursor = database.rawQuery(getAllNotes, null)
        // 迭代游标,获取note数据
        while (cursor.moveToNext()) {
            val _id = cursor.getInt(0)
            val title = cursor.getString(1)
            val content = cursor.getString(2)
            val date_created = cursor.getString(3)
            val date_updated = cursor.getString(4)
            // 将以上信息封装为一个Note实体,添加到note数组中
            notes.add(Note(_id, title, content, date_created, date_updated))
        }
        cursor.close() // 关闭游标
        database.close() // 关闭数据库
//        Log.d("notes",notes.joinToString())
        return notes // 将note数组返回
    }

    // 通过这个方法来查询符合关键字的note 数据
    // 根据 recycle_status 决定是查询回收的还是未被回收的笔记
    // recycle_status = 0 代表未回收, recycle_status = 1 代表已回收
    fun queryNotesData(context: Context?, keyWord: String, recycle_status: Int): MutableList<Note> {
        val dbHelper = getInstance(context)
        // 获取数据库
        val database = dbHelper!!.readableDatabase
        // 查询符合关键字的note的sql语句
        val queryNotesData = "select * from note " +
                "where (title like \"%" + keyWord + "%\" " +
                "or content like \"%" + keyWord + "%\") " +
                "and recycle_status = " + recycle_status +
                " order by date_updated desc"
        val notes: MutableList<Note> = ArrayList() // 存放note数据
        // 记录返回的游标
        val cursor = database.rawQuery(queryNotesData, null)
        // 迭代游标,获取note数据
        while (cursor.moveToNext()) {
            val _id = cursor.getInt(0)
            val title = cursor.getString(1)
            val content = cursor.getString(2)
            val date_created = cursor.getString(3)
            val date_updated = cursor.getString(4)
            // 将以上数据封装为Note实体,并添加到note数组中
            notes.add(Note(_id, title, content, date_created, date_updated))
        }
        cursor.close() // 关闭游标
        database.close() // 关闭数据库
        return notes // 返回note数组
    }

    // 添加一条笔记
    fun addNote(context: Context?, note: Note) {
        val dbHelper = getInstance(context)
        // 获取数据库
        val database = dbHelper!!.writableDatabase
        // 获取当前时间作为创建时间和更新时间
        val date = currentTime
        // 添加笔记的sql语句
        val insert_note = "insert into note" +
                "(title, content, date_created, date_updated)" +
                "values" +
                "('" + note.title + "','" +
                note.content + "','" +
                date + "','" + date + "')"
        database.execSQL(insert_note) // 执行sql语句
        database.close() // 关闭数据库
    }

    // 更新一条笔记
    fun updateNote(context: Context?, note: Note) {
        val dbHelper = getInstance(context)
        // 获取数据库
        val database = dbHelper!!.writableDatabase
        // 获取当前时间作为更新时间
        val date = currentTime
        // 更新笔记的sql语句
        val update_note = "update note " +
                "set title = '" + note.title + "'," +
                "content = '" + note.content + "'," +
                "date_updated = '" + date + "'" +
                "where _id = " + note.get_id()
        database.execSQL(update_note) // 执行sql语句
        database.close() // 关闭数据库
    }

    // 更改笔记回收状态
    fun changeNoteRecycleStatus(context: Context?, _id: Int) {
        val dbHelper = getInstance(context)
        // 获取数据库
        val database = dbHelper!!.writableDatabase
        // 根据 _id 更改笔记回收状态
        val change_recycle_status = "update note " +
                "set recycle_status = 1 - recycle_status " +
                "where _id = " + _id
        database.execSQL(change_recycle_status) // 执行
        database.close() // 关闭数据库
    }

    // 删除一条笔记
    fun deleteNote(context: Context?, _id: Int) {
        val dbHelper = getInstance(context)
        // 获取数据库
        val database = dbHelper!!.writableDatabase
        // 根据_id删除笔记的sql语句
        val delete_note = "delete from note " +
                "where _id = " + _id
        database.execSQL(delete_note) // 执行sql语句
        database.close() // 关闭数据库
    }

    // 删除所有笔记（给同步功能使用）
    fun deleteAllNote(context:Context?){
        val dbHelper = getInstance(context)
        //获取数据库
        val database = dbHelper!!.writableDatabase
        val sql = "delete from note where recycle_status = 0"
        database.execSQL(sql)
        database.close()  //关闭数据库
    }

    // 删除所有垃圾
    fun deleteAllTrash(context:Context?){
        val dbHelper = getInstance(context)
        //获取数据库
        val database = dbHelper!!.writableDatabase
        val sql = "delete from note where recycle_status = 1"
        database.execSQL(sql)
        database.close()  //关闭数据库
    }

    // 添加待办
    fun add_in_abeyance(context: Context?, inAbeyance: InAbeyance): Int {
        val dbHelper = getInstance(context)
        // 获取数据库
        val database = dbHelper!!.writableDatabase
        // 添加待办的sql语句
        val add_in_abeyance = "insert into in_abeyance " +
                "(content, date_remind, date_created)" +
                "values" +
                "('" + inAbeyance.content + "','" +
                inAbeyance.date_remind + "','" +
                inAbeyance.date_created +
                "')"
        database.execSQL(add_in_abeyance) // 执行sql
        val get_id = "select last_insert_rowid() from in_abeyance"
        val cursor = database.rawQuery(get_id, null)
        var _id = -1
        if (cursor.moveToFirst()) {
            _id = cursor.getInt(0)
        }
        cursor.close() // 关闭游标
        database.close() // 关闭数据库
        return _id // 返回 _id
    }

    // 获取全部待办信息
    fun getInAbeyanceData(context: Context?): MutableList<InAbeyance> {
        val dbHelper = getInstance(context)
        // 获得数据库
        val database = dbHelper!!.writableDatabase
        val get_all_in_abeyance = "select * from in_abeyance " +
                "order by _id desc"
        val cursor = database.rawQuery(get_all_in_abeyance, null)
        val inAbeyanceList: MutableList<InAbeyance> = ArrayList()
        while (cursor.moveToNext()) {
            val _id = cursor.getInt(0)
            val content = cursor.getString(1)
            val date_remind = cursor.getString(2)
            val status = cursor.getInt(3)
            val inAbeyance = InAbeyance(_id, content, date_remind, status)
            inAbeyanceList.add(inAbeyance)
        }
        cursor.close() // 关闭游标
        database.close() // 关闭数据库
        return inAbeyanceList // 返回全部待办
    }

    // 根据关键字查询待办
    fun queryInAbeyanceData(context: Context?, keyWord: String): MutableList<InAbeyance> {
        val dbHelper = getInstance(context)
        // 获得数据库
        val database = dbHelper!!.readableDatabase
        // 编写按关键字查询的sql语句
        val query_in_abeyance = "select * from in_abeyance " +
                "where content like \"%" + keyWord + "%\"" +
                "order by _id desc"
        val cursor = database.rawQuery(query_in_abeyance, null) // 执行sql语句
        val inAbeyanceList: MutableList<InAbeyance> = ArrayList()
        while (cursor.moveToNext()) {
            val _id = cursor.getInt(0)
            val content = cursor.getString(1)
            val date_remind = cursor.getString(2)
            val status = cursor.getInt(3)
            val inAbeyance = InAbeyance(_id, content, date_remind, status)
            inAbeyanceList.add(inAbeyance)
        }
        Log.e("xxx", "queryInAbeyanceData: $inAbeyanceList")
        cursor.close() // 关闭游标
        database.close() // 关闭数据库
        return inAbeyanceList
    }

    // 删除一条待办
    fun deleteInAbeyance(context: Context?, _id: Int) {
        val dbHelper = getInstance(context)
        // 获取数据库
        val database = dbHelper!!.writableDatabase
        // 删除待办的sql语句
        val deleteInAbeyance = "delete from in_abeyance where " +
                "_id = " + _id
        database.execSQL(deleteInAbeyance) // 执行sql语句
        database.close() // 关闭数据库
    }

    // 修改待办状态
    fun changeInAbeyanceStatus(context: Context?, _id: Int) {
        val dbHelper = getInstance(context)
        // 获取数据库
        val database = dbHelper!!.writableDatabase
        // 更新待办完成状态的sql语句
        val update_in_abeyance_status = "update in_abeyance set " +
                "status = 1 - status where " +
                "_id = " + _id
        database.execSQL(update_in_abeyance_status) // 执行sql语句
        database.close() // 关闭数据库
    }

    // 更新一条待办
    fun updateInAbeyance(context: Context?, inAbeyance: InAbeyance) {
        val dbHelper = getInstance(context)
        // 获取数据库
        val database = dbHelper!!.writableDatabase
        // 更新待办的sql语句
        val update_in_abeyance = "update in_abeyance set " +
                "content = '" + inAbeyance.content + "'," +
                "date_remind = '" + inAbeyance.date_remind + "'" +
                " where " +
                "_id = " + inAbeyance.get_id()
        database.execSQL(update_in_abeyance) // 执行sql语句
        database.close() // 关闭数据库
    }
}