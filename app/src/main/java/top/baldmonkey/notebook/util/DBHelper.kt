package top.baldmonkey.notebook.util

import android.content.Context
import android.database.sqlite.SQLiteDatabase.CursorFactory
import android.database.sqlite.SQLiteOpenHelper
import android.database.sqlite.SQLiteDatabase
import top.baldmonkey.notebook.util.DBHelper
import kotlin.jvm.Synchronized

/**
 * 这是继承自 SQLiteOpenHelper 的
 * 自定义的 MyDBHelper
 * 可以通过这个类获取数据库
 */
class DBHelper  // 私有化构造函数
private constructor(context: Context?, name: String?, factory: CursorFactory?, version: Int) :
    SQLiteOpenHelper(context, name, factory, version) {
    // 数据库初始化
    override fun onCreate(sqLiteDatabase: SQLiteDatabase) {
        // 创建note表的sql语句
        val create_table_note = "create table note(" +
                "_id integer primary key autoincrement," +
                "title text," +
                "content text," +
                "date_created text," +
                "date_updated text," +
                "recycle_status integer default 0)"
        // 执行
        sqLiteDatabase.execSQL(create_table_note)

        // 插入一个默认笔记,作为用户使用手册
        val insert_default_note = """
             insert into note (title, content, date_created, date_updated)values("使用手册", "1.本软件可以编写笔记，也可以创建待办事件;
             2.页面提供有相应的搜索框来搜索内容;
             3.点击右下角按钮可以进行添加操作;
             4.点击笔记或待办可以进行修改操作;
             5.长按笔记将笔记移入回收站;
             6.长按待办可以进行删除操作;
             7.设置了提醒日期的待办,会在指定时间提醒;
             8.点击待办左边的选择框,可以改变待办的完成状态;
             9.在回收站页面可以按照关键字查询已回收笔记;
             10.在回收站页面点击笔记可以将笔记还原;
             11.在回收站页面长按笔记可以将笔记彻底删除;
             ","06月20日 上午 11:11","06月20日 上午 11:11")
             """.trimIndent()
        sqLiteDatabase.execSQL(insert_default_note)

        // 创建in_abeyance表的sql语句
        val create_table_in_abeyance = "create table in_abeyance (" +
                "_id integer primary key autoincrement," +
                "content text," +
                "date_remind text," +
                "status integer default 0," +
                "date_created text)"
        // 执行
        sqLiteDatabase.execSQL(create_table_in_abeyance)
    }

    // 数据库升级
    override fun onUpgrade(sqLiteDatabase: SQLiteDatabase, i: Int, i1: Int) {}

    companion object {
        private const val DBNAME = "note_book.db" // 数据库名称
        private var instance // 实例
                : DBHelper? = null

        // 对外提供获取实例的方法
        @JvmStatic
        @Synchronized
        fun getInstance(context: Context?): DBHelper? {
            if (instance == null) {
                instance = DBHelper(context, DBNAME, null, 1)
            }
            return instance
        }
    }
}