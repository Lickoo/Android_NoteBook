package top.baldmonkey.notebook.util

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import top.baldmonkey.notebook.bean.Note
import java.io.IOException

class GetRequestAsyncTask : AsyncTask<String, Void, String>() {

    override fun doInBackground(vararg params: String): String {
        val url = params[0]

        val client = OkHttpClient()
        val request = Request.Builder()
            .url(url)
            .build()

        try {
            val response: Response = client.newCall(request).execute()
            return response.body?.string() ?: ""
        } catch (e: IOException) {
            e.printStackTrace()
            return "请求异常：${e.message}"
        }
    }

    override fun onPostExecute(result: String) {
        if (result.startsWith("请求异常")) {
            // 处理错误情况
        } else {
            // 处理请求成功的结果
        }
    }
}

class DataDownloader {
    fun DownloadNotes(context:Context):MutableList<Note>{
        val url = "http://mcljy.wicp.net/getjson/" // 请求的 URL
        val getRequestAsyncTask = GetRequestAsyncTask()
        val get_result = getRequestAsyncTask.execute(url).get()
        Toast.makeText(context,"下载请求已发送", Toast.LENGTH_SHORT).show()
        return JsonToObj(get_result)
    }

    fun JsonToObj(json:String):MutableList<Note>{
        val gson = Gson()
        val NotesList: MutableList<Note> = gson.fromJson(json, Array<Note>::class.java).toMutableList()
        // 处理转换后的对象列表
//        for (note in NotesList) {
//            Log.d("ID: ${note._id}", "Name: ${note.title}")
//        }
        return NotesList
    }
}