package top.baldmonkey.notebook.util

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson
import top.baldmonkey.notebook.bean.Note
import top.baldmonkey.notebook.util.DBOperator.getNotesData
import java.io.IOException
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response


class HttpPostTask(private val url: String, private val jsonData: String) : AsyncTask<Unit, Unit, String>() {

    override fun doInBackground(vararg params: Unit): String {
        val client = OkHttpClient()

        val mediaType = "application/json; charset=utf-8".toMediaType()
        val requestBody = jsonData.toRequestBody(mediaType)
        val request = Request.Builder()
            .url(url)
            .post(requestBody)
            .build()

        try {
            val response = client.newCall(request).execute()
            return if (response.isSuccessful) {
                response.body?.string() ?: ""
            } else {
                "请求失败，错误码：${response.code}"
            }
        } catch (e: IOException) {
            e.printStackTrace()
            return "请求异常：${e.message}"
        }
    }

    override fun onPostExecute(result: String) {
        // 处理请求结果
        println("请求结果：$result")
    }
}

class DataUpdater {
    private lateinit var notesData:MutableList<Note>

    fun Update(context: Context){
        notesData = getNotesData(context,0)
        val jsonData = ObjToJson(notesData) // 请求的 JSON 数据
        val task = HttpPostTask("http://mcljy.wicp.net/", jsonData)
        task.execute()
        Toast.makeText(context,"上传请求已发送",Toast.LENGTH_SHORT).show()
    }

    fun ObjToJson(obj:Any):String{
        val gson = Gson()
        return gson.toJson(obj)
    }

}