package top.baldmonkey.notebook.util

import android.annotation.SuppressLint
import androidx.annotation.RequiresApi
import android.os.Build
import android.util.Log
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * 这是一个获取日期的工具类
 */
object DateUtil {
    // 设置格式
    // 返回日期字符串
    // 获取当前日期,并返回一个日期字符串
    @JvmStatic
    val currentTime: String
        get() {
            // 设置格式
            @SuppressLint("SimpleDateFormat") val dateFormat: DateFormat =
                SimpleDateFormat("MM月dd日 aa HH:mm")
            // 返回日期字符串
            return dateFormat.format(Date())
        }

    // 传入一个Date类型对象,转换为字符串
    fun getFormatTime(date: Date?): String {
        // 设置格式
        @SuppressLint("SimpleDateFormat") val simpleDateFormat = SimpleDateFormat("MM月dd日 aa HH:mm")
        return simpleDateFormat.format(date)
    }

    // 传入一个util包下Date类型日期时间，返回对应的绝对时间(long类型的毫秒数)
    @RequiresApi(api = Build.VERSION_CODES.O)
    fun getTimeMillis(dateTime: String?): Long {
        @SuppressLint("SimpleDateFormat") val dateFormat: DateFormat =
            SimpleDateFormat("MM月dd日 aa HH:mm")
        try {
            val date = dateFormat.parse(dateTime)
            // 设置年份当前年份
            // 由于java.util.Date类中年份从1900年开始,需要 - 1900
            date.year = Calendar.getInstance()[Calendar.YEAR] - 1900
            return date.time
        } catch (e: ParseException) {
            Log.e("DateUtil->", "getTimeMillis: " + "捕获异常")
        }
        return System.currentTimeMillis() + 5000
    }
}