package top.baldmonkey.notebook.util

import android.app.Notification
import android.content.BroadcastReceiver
import androidx.annotation.RequiresApi
import android.os.Build
import android.content.Intent
import android.app.NotificationManager
import top.baldmonkey.notebook.activity.MainActivity
import android.app.PendingIntent
import android.os.Bundle
import android.app.NotificationChannel
import android.content.Context
import top.baldmonkey.notebook.R

/**
 * 这是继承自 BroadcastReceiver 的
 * 自定义的 AlarmReceiver 广播接收器
 * 用于向用户发出待办提醒
 */
class AlarmReceiver : BroadcastReceiver() {
    @RequiresApi(api = Build.VERSION_CODES.M)
    override fun onReceive(context: Context, intent: Intent) {
        // 获取系统通知服务
        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        // 创建意图
        val intent1 = Intent(context, MainActivity::class.java)
        // 创建点击通知后的跳转意图
        val pendingIntent = PendingIntent.getActivity(
            context, 0, intent1, PendingIntent.FLAG_IMMUTABLE)
        // 创建通知
        val notification: Notification
        val bundle = intent.extras
        // 判断如果为安卓8.0以上设置频道id
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // 创建频道id
            val notificationChannel = NotificationChannel(
                "BaldMonkey", "待办提醒",
                NotificationManager.IMPORTANCE_HIGH) // 弹出并在通知栏显示
            // 给通知管理器创建频道id
            notificationManager.createNotificationChannel(notificationChannel)
            // 实例化通知,channelId与上面设置的一致
            notification = Notification.Builder(context, "BaldMonkey")
                .setSmallIcon(R.drawable.ic_baseline_alarm_24)
                .setTicker("待办提醒")
                .setContentTitle("待办事项提醒")
                .setContentText(bundle!!.getString("content"))
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build()
            // 发送通知
            notificationManager.notify(3, notification)
        }
    }
}